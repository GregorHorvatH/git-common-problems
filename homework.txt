PART 1
- create and switch to branch "feature2";
- create file "home-work-02.txt";
- add line "first line";
- commit changes;
- add line "second line";
- commit changes;
- add line "third line";
- commit changes;
- push commits to the server;

PART 2
- squash 3 commits into 1 (reset, commit --amend);
- push changes to the server;

PART 3
- update README.txt in "main" branch, change "line 2" to "line 2 - main";
- commit changes;
- push changes to the server;

- update README.txt in "feature2" branch, change "line 2" to "line 2 - feature";
- commit changes;
- push changes to the server;

- rebase "feature2" to "main";
- fix merge conflicts;
- commit/push changes to the server;
